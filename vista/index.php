<?php
require_once '../modelo/claseConsulta.php';
$consulta = new Consulta();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
</head>

<body>
    <?php
    $filas = $consulta->obtenerPersona();
    foreach ($filas as $fila) :
    ?>
        <table>
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>DNI</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $fila['nombre']; ?></td>
                    <td><?php echo $fila['apellido']; ?></td>
                    <td><?php echo $fila['dni']; ?></td>
                </tr>
            </tbody>
        </table>

    <?php
    endforeach;
    ?>

</body>

</html>
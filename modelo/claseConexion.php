<?php

class Conexion
{
    private $user;
    private $pass;
    private $host;

    private $db;

    public function __construct()
    {
        $this->user = "root";
        $this->pass = "";
        $this->host = "localhost";
        $this->db = "legajos";
    }
    public function obtenerConexion()
    {

        try {
            $con = new PDO("mysql:dbname=$this->db;host=$this->host;charset=utf8", $this->user, $this->pass);
        } catch (PDOException $e) {
            echo "Fallo en la conexion " . $e;
        }
        return $con;
    }
}
